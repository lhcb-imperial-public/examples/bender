# Bender

This is a tutorial on how to use Bender to access reconstructed data/MC, and combine the reconstructed objects into particles.


## Running

   * lb-run -c best Bender/v35r6 bash
   * python BenderAnalysis.py /vols/lhcb/masmith/gangadir_bs2kktautau/workspace/mesmith/LocalXML/59/0/output/Brunel.dst 

By default all events in the all the files passed on the command line are processed. To run over a smaller number for testing make the last argument to the script the number of events to run over.

## Ouput

This script will produce a root file in the current directory with the same name as the first input file with the substitution of Brunel with Bender. This file contains a ROOT file with the MC truth and reconstructed information for all events with a reconstructable final state.


## Looking at the code
The part of the code to look at first is the module Bs2Phi3TauTau. This contains a class that is called once per event, with access to all the MC truth, and reconstructed information for that event.
